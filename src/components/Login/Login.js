import React, { Component } from "react";
import './Login.css'; 
import { Link } from 'react-router-dom';

class Login extends Component {
    render() {
        return (
          <div className="LoginView" >
            <form>
                <h1>Logo</h1>
                <div className="formview">
                <UserInput />
                <PasswordInput />
                <Checkbox />
              <div>
                <button type="submit">Sign In</button>
              </div>
                <LinkReg/>
              </div>
            </form>
          </div>
        )
      }
      
    }
    
    class UserInput extends React.Component {
      
      constructor(props) {
        super(props);
        
        this.onChange = this.onChange.bind(this);
      }
      
      onChange() {
        console.log('change username');
      }
      
      render() {
        return (
          <InputContainer onChange={ this.onChange } type="text"  />
        )
      }
      
    }
    
    class PasswordInput extends React.Component {
      
      render() {
        return (
          <InputContainerr type="password" />
        )
      }
      
    }
    
    class InputContainer extends React.Component {
      
      render () {
        return (
          <div className="InputContainer">
            <input placeholder="Username" onChange={this.props.onChange} type={ this.props.type } required/>
          </div>
        );
      }    
}
class InputContainerr extends React.Component {
      
    render () {
      return (
        <div className="InputContainerr">
          <input placeholder="Password" onChange={this.props.onChange} type={ this.props.type } required/>
        </div>
      );
    }    
}

class Checkbox extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        isChecked: false,
      };
    }
    toggleChange = () => {
      this.setState({
        isChecked: !this.state.isChecked,
      });
    }
    render() {
      return (
        <label className="InputCheckbox" >
          <input type="checkbox" 
            checked={this.state.isChecked}
            onChange={this.toggleChange}
          />
          <p>Remember Me!</p> {' '} <LinkPass/>
         </label>
      );
    }
  }
  class LinkPass extends React.Component {
  
    render() { 
      return (
          <div className="LinkPass">
            <a href="/" className="forgpass">Forgot Password?</a>
          </div>
      )
    }
  }
  class LinkReg extends React.Component {
  
    render() { 
      return (
          <div className="LinkReg">
            Don't have account? <a href="/">Register</a>
          </div>
      )
    }
  }

export default Login;